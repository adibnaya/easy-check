<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Rate;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::paginate(10);
        return view('transactions.index', compact('transactions'));
    }

    public function indexSearch(Request $request)
    {
        $transactions = Transaction::where('account', 'LIKE', "%$request->search_string%")
            ->orWhere('bank_branch', 'LIKE', "%$request->search_string%")
            ->orWhere('check_num', 'LIKE', "%$request->search_string%")
            ->orWhere('bank_num', 'LIKE', "%$request->search_string%")
            ->orWhereHas('customer', function ($q) use ($request) {
                $q->where('name', 'LIKE', "%$request->search_string%")
                    ->orWhere('id_num', 'LIKE', "%$request->search_string%");
            })->paginate(10);
        return view('transactions.index', compact('transactions'));
    }

    public function indexsorted($by)
    {
        $users = User::all();
        $customers = Customer::all();
        $transactions = Transaction::query()->orderBy($by)->paginate(10);
        return view('transactions.index', compact('transactions', 'customers', 'users'));
    }

    public function indexfiltered($by, $what)
    {
        $users = User::all();
        $customers = Customer::all();
        $transactions = Transaction::query()->where($by, $what)->paginate(10);
        return view('transactions.index', compact('transactions', 'customers', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $customers = Customer::all();
        return view('transactions.create', compact('users', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new Transaction();
        if (auth()->user()->isAdmin()) {
            $transaction->user_id = $request->user_id;
        } else {
            $transaction->user_id = auth()->user()->id;
        }
        $transaction->customer_id = $request->customer_id;
        $transaction->account = $request->account;
        $transaction->bank_branch = $request->bank_branch;
        $transaction->bank_num = $request->bank_num;
        $transaction->check_num = $request->check_num;
        $transaction->save();
        return redirect('transactions');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::findOrFail($id);
        return view('transactions.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::findOrFail($id);
        $users = User::all();
        $customers = Customer::all();
        return view('transactions.edit', compact('transaction', 'users', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->update($request->all());
        return redirect('transactions');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->delete();
        return redirect('transactions');
    }

    public function bulkdelete(Request $req)
    {
        $id = $req->id;
        foreach ($id as $transaction) {
            $transaction = Transaction::find($transaction);
            $transaction->delete();
        }
        return redirect()->back()->with('message', 'Successfully Delete Your Multiple Selected Records.');;
    }
}
